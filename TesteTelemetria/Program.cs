﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TesteTelemetria
{
    class Program
    {
        public static void StartClient()
        {
            // Data buffer for incoming data.  
            byte[] bytes = new byte[1024];
            double counter = 0;
            // Connect to a remote device.  
            try
            {
                IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse("177.92.12.98"), 90);
                
                // Create a TCP/IP  socket.  
                Socket sender = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                // Connect the socket to the remote endpoint. Catch any errors.  
                try
                {
                    sender.Connect(remoteEP);

                    Console.WriteLine("Socket connected to {0}", sender.RemoteEndPoint.ToString());
                    byte[] msg = Encoding.ASCII.GetBytes("{ \"status\":\"connect\", \"username\":\"digifort\", \"password\":\"d1g1f0rt@2019\"}");
                    Console.WriteLine("send message:{ \"status\":\"connect\", \"username\":\"digifort\", \"password\":\"d1g1f0rt@2019\"}");
                    int bytesSent = sender.Send(msg);
                    DateTime outputTime = DateTime.Now;
                    int value = 0;

                    while(true)
                    {
                        int bytesRec = sender.Receive(bytes);
                        string answer = Encoding.ASCII.GetString(bytes, 0, bytesRec);
                        counter++;
                        Console.WriteLine("Answer:" + answer);
                        Thread.Sleep(500);

                        if (DateTime.Now > outputTime.AddSeconds(5))
                        {
                            if (value == 0)
                                value = 1;
                            else
                                value = 0;

                            msg = Encoding.ASCII.GetBytes("{\"status\":\"output\", \"OUT1\":\"" + value.ToString() + "\", \"OUT2\":\"0\", \"OUT3\":\"0\", \"OUT4\":\"0\", \"OUT5\":\"0\", \"OUT6\":\"0\", \"OUT7\":\"0\", \"OUT8\":\"0\", \"OUT9\":\"0\", \"OUT10\":\"0\"}");
                            Console.WriteLine("send message:" + "{\"status\":\"output\", \"OUT1\":\"" + value.ToString() + "\", \"OUT2\":\"0\", \"OUT3\":\"0\", \"OUT4\":\"0\", \"OUT5\":\"0\", \"OUT6\":\"0\", \"OUT7\":\"0\", \"OUT8\":\"0\", \"OUT9\":\"0\", \"OUT10\":\"0\"}");
                            bytesSent = sender.Send(msg);

                            answer = Encoding.ASCII.GetString(bytes, 0, bytesRec);
                            Console.WriteLine("Answer:" + answer);
                            Thread.Sleep(500);
                            outputTime = DateTime.Now;
                        }

                        if (answer.Contains("alarm"))
                        {
                            msg = Encoding.ASCII.GetBytes("{ \"status\":\"received\"}");
                            Console.WriteLine("send message:{ \"status\":\"received\"}");
                            bytesSent = sender.Send(msg);
                        }

             


                    }

  
                    sender.Shutdown(SocketShutdown.Both);
                    sender.Close();

                }
                catch (ArgumentNullException ane)
                {
                    Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
                }
                catch (SocketException se)
                {
                    Console.WriteLine("SocketException : {0}", se.ToString());
                }
                catch (Exception e)
                {
                    Console.WriteLine("Unexpected exception : {0}", e.ToString());
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static int Main(String[] args)
        {
            StartClient();
            return 0;
        }
    }

    public class StateObject
    {
        // Client socket.  
        public Socket workSocket = null;
        // Size of receive buffer.  
        public const int BufferSize = 256;
        // Receive buffer.  
        public byte[] buffer = new byte[BufferSize];
        // Received data string.  
        public StringBuilder sb = new StringBuilder();
    }
}
